const {perfis, proximoId} = require('../../data/db');

function indicePerfil(filtro){
    if(!filtro) return -1;

    const {id, nome} = filtro;
    if(id) {
        return perfis.findIndex(perfil => perfil.id === id);
    }
    else if (nome){
        return perfis.findIndex(perfil => perfil.nome === nome);
    }

    return -1;
}

module.exports = {
    novoPerfil(_, {nome}) {
        const perfilExistente = perfis
            .some(perfil => perfil.nome === nome);

        if(perfilExistente){
            throw new Error('Perfil já cadastrado');
        }

        const novo = {
            id: proximoId(),
            nome
        };

        perfis.push(novo);
        return novo;
    },

    excluirPerfil(_, {filtro}){
        const i = indicePerfil(filtro);

        if( i < 0 ) return  null

        const excluidos = perfis.splice(i, 1);

        return excluidos ? excluidos[0] : null;
    },

    alterarPerfil(_, {filtro, dados}){
        const i = indicePerfil(filtro)

        if (i < 0) return null;

        const perfil = {
            ...perfis[1],
            ...dados
        }

        perfis.splice(i, 1, perfil);
        return perfil;
    }
}
